<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('FirstName',50);
            $table->string('SecondName',50);
            $table->string('Surname',50);
            $table->string('SecondSurname',50);
            $table->string('email',50)->unique();
            $table->string('Cedula',20)->unique();
            $table->string('NumeroINSS',20)->unique();
            $table->date('DateOfBirth');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
