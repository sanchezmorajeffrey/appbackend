<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $casts = [
        'status' => 'boolean',
    ];

    protected $fillable = [
        'FirstName',
        'SecondName',
        'Surname',
        'SecondSurname',
        'email',
        'Cedula',
        'NumeroINSS',
        'DateOfBirth',
        'status'
    ];
}
