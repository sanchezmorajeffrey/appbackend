<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employee = Employee::all();
        return response()->json([
            'data' => $employee
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages = [
            'required' => 'El campo :attribute es obligatorio.',
            'unique' => 'El :attribute ya ha sido registrado.'
           ];

        $rules = [
            'FirstName' => 'required',
            'FirstName' => 'required',
            'Surname' => 'required',
            'Cedula' => 'required|unique:employees',
            'DateOfBirth' => 'required',
            'email'=> 'required|email|unique:employees',
            'status'  => 'required',
            'NumeroINSS' => 'required|unique:employees'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 'validacion_datos_error',
            "lista_errores"=>$validator->errors()->all()], 400);
        }
        $employee = Employee::create($request->all());
        return response()->json([
            'data' => $employee
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $employee = Employee::findOrFail($id);
        return response()->json([
            'data' => $employee
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $messages = [
            'required' => 'El campo :attribute es obligatorio.',
            'unique' => 'El :attribute ya ha sido registrado.'
           ];
        $rules = [
            'FirstName' => 'required',
            'FirstName' => 'required',
            'Surname' => 'required',
            'DateOfBirth' => 'required',
            'status'  => 'required',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 'validacion_datos_error',
            "lista_errores"=>$validator->errors()], 400);
        }
        $employee = Employee::findOrFail($id)->update($request->all());
        return response()->json([
            'data' => 'Empleado actualizado!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Employee::destroy($id);
        return response()->json([
            'data' => 'Empleado eliminado!'
        ], 200);
    }

}
